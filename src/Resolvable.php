<?php

namespace JournoLink\Resolvable;

trait Resolvable
{
    /**
     * Resolve a Model instance from the given data
     *
     * @param  mixed  $parameter
     * @return \Illuminate\Database\Eloquent\Model
     */
    public static function resolve($parameter)
    {
        // Firstly we'll check if the provided parameter is an instance of the called
        // class. If so, we can simply return the parameter as a resolved instance
        // of itself. This saves trying to resolve a Model that's already done
        if (is_object($parameter) && get_class($parameter) == get_called_class()) {
            return $parameter;
        }

        $model = new static;

        // We want to make sure that the Model unique key is always included in the
        // resolvable fields, whether or not the developer has included it in the
        // resolvable array. This ensures that resolve can be used as find() is
        $resolvable = array_merge([$model->getKeyName()], $model->resolvable ?? []);

        $search = $model->query();

        foreach ($resolvable as $field) {
            $search->orWhere($field, (string) $parameter);
        }

        // We only want to return an instance if our search managed to return a single
        // result. If there are more than 1 result returned, then we'll treat it as
        // if no result was returned at all and follow on to returning null value
        if ($search->count() == 1) {
            return $search->first();
        }

        return null;
    }
}
